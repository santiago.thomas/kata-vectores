# Vectors Kata : AI Challenge
 
Bienvenidos al UdeSA AI Challenge! En este desafío te retamos a crear tu propio equipo de fútbol y competir con otros equipos participantes.
 
- [AI Challenge](#ai-challenge)
  - [Introducción](#introducción)
  - [Reglas](#reglas)
  - [Construí tu equipo](#construí-tu-equipo)

## Introducción
Te presentamos el AI challenge, el objetivo del evento es programar la inteligencia artificial de los jugadores, usando tácticas, jugadas, mentalidad de equipo o individual, etc.

## Reglas
- No se pueden usar métodos que interactúen directamente con el motor físico del juego. 
- Solo se pueden usar como lugar de inicio para los jugadores estas 9 [posiciones]: *A1, A2, A3, B1, B2, B3, C1, C2, C3*  y estas deben ser distintas para cada jugador del equipo.

## Construí tu equipo
En [Guia de Programación](GUIA.md) te ayudaremos a familiarizarte con el juego y a crear tu primer equipo!
