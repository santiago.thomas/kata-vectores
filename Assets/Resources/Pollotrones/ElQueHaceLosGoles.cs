﻿using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Resources.Pollotrones
{
    public class ElQueHaceLosGoles : TeamPlayer
    {
        private const int MitadDeChancha = 10;
        private const int DistanciaDeDisparo = 7;

        public override void OnUpdate()
        {
            if (GetBallDistanceFromMyGoal() > MitadDeChancha)
                Atacar();
            else
                IrAPescar();
        }

        private void IrAPescar()
        {
            GoTo(GetFishingPosition());
        }

        private void Atacar()
        {
            MoveBy(GetDirectionTo(GetBallPosition()) * GetBallVelocity().magnitude);
        }

        public override void OnReachBall()
        {
            if (!FacingRightDirection())
            {
                ShootBall(GetDirectionTo(GetPositionFor(FieldPosition.B2)),ShootForce.Medium);
            }
            if (GetDistanceToRivalGoal() > DistanciaDeDisparo) 
                ShootBall(GetDirectionTo(GetPositionFor(GetFishingPosition())),ShootForce.Low);
            else
            {
                ShootBall(GetDirectionTo(GetRivalGoalPosition()),ShootForce.High);
            }
        }

        private bool FacingRightDirection()
        {
            var direction = GetRivalGoalPosition() - GetPosition();
            
            var ang = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            var anglecurrent = Quaternion.Euler(GetPosition()).eulerAngles.z *-1;
            float checkAngle = 0;
 
            if (anglecurrent > 0 && anglecurrent <= 90)
            {
                checkAngle = ang - 90;
            }
            if (anglecurrent > 90 && anglecurrent <= 360)
            {
                checkAngle = ang + 270;
            }
            
            var result = anglecurrent <= checkAngle + 0.5f && anglecurrent >= checkAngle - 0.5f;

            return result;
        }

        private float GetDistanceToRivalGoal()
        {
            return Vector3.Distance(GetRivalGoalPosition(), GetPosition());
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {

        }

        public override FieldPosition GetInitialPosition() => FieldPosition.C2;

        public override string GetPlayerDisplayName() => "El Golitos!";
        
        private FieldPosition GetFishingPosition()
        {
            if(GetBallPosition().z > 3)
                return FieldPosition.E1;
            if (GetBallPosition().z < -3)
                return FieldPosition.E3;
            return FieldPosition.E2;
        }
        
        private float GetBallDistanceFromMyGoal()
        {
            return Vector3.Distance(GetBallPosition(),GetMyGoalPosition());
        }
    }
}