using Core.Games;
using Core.Player;
using JetBrains.Annotations;
using UnityEngine;

namespace Resources.Pollotrones
{
    [UsedImplicitly]
    public class Pollotrones : Team
    {
        public TeamPlayer GetPlayerOne() => new ElUno();

        public TeamPlayer GetPlayerTwo() => new ElMid();

        public TeamPlayer GetPlayerThree() => new ElQueHaceLosGoles();
        
        public Color PrimaryColor => new Color(1f, 0.87f, 0.42f);

        public string GetName() => "Pollotrones";

        public string TeamShield => "Pink";
    }
}