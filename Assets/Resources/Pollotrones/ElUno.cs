﻿using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Resources.Pollotrones
{
    public class ElUno : TeamPlayer
    {
        private const int MitadDeCancha = 10;
        
        public override void OnUpdate()
        {
            if (GetBallDistanceFromMyGoal() > MitadDeCancha)
                AlArco();
            else
                Defender();
        }
        
        private void AlArco()
        {
            if(GetBallVelocity().magnitude > 15)
                MoveBy(GetDirectionTo(GetBallPosition()));
            else
                GoTo(GetArcoPosition());
        }

        private void Defender()
        {
            if(GetBallVelocity().magnitude > 8)
                MoveBy(GetDirectionTo(GetBallPosition()));
            else
            {
                MoveBy(GetDirectionTo(GetBallPosition()));
            }
        }

        public override void OnReachBall()
        {
            ShootBall(GetDirectionTo(GetPositionFor(GetSacarlaPosition())),ShootForce.High);
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {

        }

        private FieldPosition GetArcoPosition()
        {
            return FieldPosition.A2;
        }
        
        private FieldPosition GetSacarlaPosition()
        {
            if(GetBallPosition().z > 4)
                return FieldPosition.E1;
            if (GetBallPosition().z < -4)
                return FieldPosition.E3;
            return FieldPosition.E2;
        } 

        public override FieldPosition GetInitialPosition() => FieldPosition.A2;

        public override string GetPlayerDisplayName() => "El Uno";
        
        private float GetBallDistanceFromMyGoal()
        {
            return Vector3.Distance(GetBallPosition(),GetMyGoalPosition());
        }
    }
}