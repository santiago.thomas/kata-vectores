using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Resources.Pollotrones
{
    public class ElMid : TeamPlayer
    {
        private const int LimiteDeDefensa = 15;
        
        public override void OnUpdate()
        {
            if (GetBallDistanceFromMyGoal() > LimiteDeDefensa)
                Defender(); 
            else
                Atacar();
        }

        private void Defender()
        {
            GoTo(GetMidPosition());
        }

        private void Atacar()
        {
            MoveBy(GetDirectionTo(GetBallPosition()));
        }

        public override void OnReachBall()
        { 
            if(GetDistanceToRivalGoal() < 12)
                ShootBall(GetDirectionTo(GetRivalGoalPosition()), ShootForce.High);
            else
            {
                ShootBall(GetDirectionTo(GetPositionFor(GetSacarlaPosition())), ShootForce.Low);
            }
        }

        private float GetDistanceToRivalGoal()
        {
            return Vector3.Distance(GetPosition(),GetRivalGoalPosition());
        }

        private bool FacingRightDirection()
        {
            var direction = GetRivalGoalPosition() - GetPosition();
            
            var ang = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            var anglecurrent = Quaternion.Euler(GetPosition()).eulerAngles.z *-1;
            float checkAngle = 0;
 
            if (anglecurrent > 0 && anglecurrent <= 90)
            {
                checkAngle = ang - 90;
            }
            if (anglecurrent > 90 && anglecurrent <= 360)
            {
                checkAngle = ang + 270;
            }
            
            var result = anglecurrent <= checkAngle + 0.5f && anglecurrent >= checkAngle - 0.5f;

            return result;
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {
            
        }

        public override FieldPosition GetInitialPosition() => FieldPosition.B2;

        public override string GetPlayerDisplayName() => "El Mid";
        
        private FieldPosition GetMidPosition()
        {
            if(GetBallPosition().z > 5)
                return FieldPosition.B1;
            if (GetBallPosition().z < -5)
                return FieldPosition.B3;
            return FieldPosition.B2;
        }
        
        private FieldPosition GetSacarlaPosition()
        {
            if(GetBallPosition().z > 2)
                return FieldPosition.E1;
            if (GetBallPosition().z < -2)
                return FieldPosition.E3;
            return FieldPosition.E2;
        } 
        
        private float GetBallDistanceFromMyGoal()
        {
            return Vector3.Distance(GetBallPosition(),GetMyGoalPosition());
        }
    }
}