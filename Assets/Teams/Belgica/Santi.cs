﻿using Core.Games;
using Core.Player;
using Core.Utils;

namespace Teams.Belgica
{
    public class Santi : TeamPlayer
    {
        public override void OnUpdate()
        {
            GoTo(GetBallPosition());
        }

        public override void OnReachBall()
        {
            ShootBall(GetRivalGoalPosition(), ShootForce.High);
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {

        }

        public override FieldPosition GetInitialPosition() => FieldPosition.B2;

        public override string GetPlayerDisplayName() => "Santi";
    }
}