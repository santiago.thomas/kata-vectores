﻿using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Teams.Belgica
{
    public class Alan : TeamPlayer
    {
        private bool attack;
        private bool canShootToArch;

        public override void OnUpdate()
        {
            if (ShouldAttack())
            {
                Debug.Log("Debo atacar soy alan");
                if (IsCloseToArch())
                {
                    canShootToArch = true;
                }
                else
                {
                    canShootToArch = false;
                    MoveBy(GetBallPosition());
                }
            }
            else
            {
                GoTo(GetDirectionTo(GetMyGoalPosition()));
            }
        }

        private bool IsCloseToArch()
        {
            return Vector3.Distance(GetBallPosition(), GetRivalGoalPosition()) < 4;
        }

        private void ShootToArch()
        {
            ShootBall(GetDirectionTo(GetRivalGoalPosition()), ShootForce.High);
        }

        private bool ShouldAttack()
        {
            if (Vector3.Distance(GetBallPosition(), GetMyGoalPosition()) <
                Vector3.Distance(GetBallPosition(), GetRivalGoalPosition()))
                return false; 
            return true;
        }

        public override void OnReachBall()
        {
            if (canShootToArch)
                ShootToArch();
            else
                GoForward();
        }

        private void GoForward()
        {
            ShootBall(GetRivalGoalPosition(), ShootForce.Medium);
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {
            
        }

        public override FieldPosition GetInitialPosition() => FieldPosition.A2;

        public override string GetPlayerDisplayName() => "Alan";
    }
}