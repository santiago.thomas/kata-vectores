using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Teams.Belgica
{
    public class LeoTraidor : TeamPlayer
    {
        private bool despeje;
        public override void OnUpdate()
        {
            if (!despeje)
                GoTo(GetMyGoalPosition());
            if (Vector3.Distance(GetBallPosition(), GetPosition()) < 3)
            {
                GoTo(GetBallPosition());
                despeje = true;
            }
                
            else
                despeje = false;
        }

        public override void OnReachBall()
        {
            ShootBall(GetDirectionTo(GetRivalGoalPosition()), ShootForce.High);
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {

        }

        public override FieldPosition GetInitialPosition() => FieldPosition.C2;

        public override string GetPlayerDisplayName() => "Leo";
    }
}