using Core.Games;
using Core.Player;
using JetBrains.Annotations;
using UnityEngine;

namespace Teams.Belgica
{
    [UsedImplicitly]
    public class BelgicTeam : Team
    {
        public TeamPlayer GetPlayerOne() => new Alan();

        public TeamPlayer GetPlayerTwo() => new Santi();

        public TeamPlayer GetPlayerThree() => new LeoTraidor();
        
        public Color PrimaryColor => new Color(0.6f, 0.6f, 0.6f);

        public string GetName() => "Belgica";

        public string TeamShield => "Black";
    }
}