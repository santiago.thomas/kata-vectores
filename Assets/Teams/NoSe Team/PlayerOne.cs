﻿using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Teams.NoSeTeam
{
    public class PlayerOne : TeamPlayer
    {
        public override void OnUpdate()
        {
            var ballPosition = GetBallPosition();
            var directionToBall = GetDirectionTo(ballPosition);
            MoveBy(directionToBall);
        }

        public override void OnReachBall()
        {
            var rivalGoalPosition = GetRivalGoalPosition();
            var rivalGoalDirection = GetDirectionTo(rivalGoalPosition);
            ShootBall(rivalGoalDirection, ShootForce.High);
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {

        }

        public override FieldPosition GetInitialPosition() => FieldPosition.A2;

        public override string GetPlayerDisplayName() => "Player 1";
    }
}