using Core.Games;
using Core.Player;
using JetBrains.Annotations;
using UnityEngine;

namespace Teams.NoSeTeam
{
    [UsedImplicitly]
    public class ParticipantTeam : Team
    {
        public TeamPlayer GetPlayerOne() => new PlayerOne();

        public TeamPlayer GetPlayerTwo() => new PlayerTwo();

        public TeamPlayer GetPlayerThree() => new PlayerThree();
        
        public Color PrimaryColor => new Color(1.0f, 0.3f, 0.2f);

        public string GetName() => "NoSe Team";

        public string TeamShield => "Orange";


    }
}