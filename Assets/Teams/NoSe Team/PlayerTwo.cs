﻿using Core.Games;
using Core.Player;
using Core.Utils;
using UnityEngine;

namespace Teams.NoSeTeam
{
    public class PlayerTwo : TeamPlayer
    {
        public override void OnUpdate()
        {
            var ballPosition = GetBallPosition();
            if (Vector3.Distance(ballPosition, GetMyGoalPosition()) < 8) // 5 meters in unity units
            {
                MoveBy(GetDirectionTo(ballPosition));
            }
            else
            {
                var myGoalPosition = GetMyGoalPosition();
                var myGoalDirection = GetDirectionTo(myGoalPosition);
                MoveBy(myGoalDirection);
            }

        }

        public override void OnReachBall()
        {
            var rivalGoalPosition = GetRivalGoalPosition();
            var rivalGoalDirection = GetDirectionTo(rivalGoalPosition);
            ShootBall(rivalGoalDirection, ShootForce.High);
        }

        public override void OnScoreBoardChanged(ScoreBoard scoreBoard)
        {

        }

        public override FieldPosition GetInitialPosition() => FieldPosition.B2;

        public override string GetPlayerDisplayName() => "Player 2";
    }
}